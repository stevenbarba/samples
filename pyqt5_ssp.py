
#!/usr/bin/env python3

from random import randint
import sys

from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QGridLayout, QGroupBox, QLineEdit
from PyQt5.QtCore import Qt, QTimer

class Window(QWidget):
  def __init__(self):
    super().__init__()
    self.pressure = 1
    self.change = .0001

    self.layout = QGridLayout()
    self.makeLayout()
    self.startTimer()

    self.setGeometry(1220,800, 200, 200)
    self.setFixedSize(200, 200)
    self.setWindowTitle('I/SSP')
    self.show()

  def makeLayout(self):
    self.valve1 = QPushButton('Valve 1 Closed')
    self.valve1.toggle()
    self.valve1.setCheckable(True)
    self.layout.addWidget(self.valve1, 0, 0)
    self.valve1.clicked.connect(lambda: self.handleButton(self.valve1, 'Valve 1'))

    self.valve2 = QPushButton('Valve 2 Closed')
    self.valve2.toggle()
    self.valve2.setCheckable(True)
    self.layout.addWidget(self.valve2, 1, 0)
    self.valve2.clicked.connect(lambda: self.handleButton(self.valve2, 'Valve 2'))

    self.layout.addWidget(QGroupBox(), 2, 0)
    self.pressureDisplay = QLineEdit(f'Pressure: {self.pressure:.4f}')
    self.pressureDisplay.setTextMargins(15, 0, 0, 0)
    self.layout.addWidget(self.pressureDisplay, 2, 0, alignment=Qt.AlignCenter)

    self.setLayout(self.layout)

  def fakePressure(self):
    self.pressure += randint(1, 3) * self.change
    self.pressureDisplay.setText(f'Pressure: {self.pressure:.4f}')

  def startTimer(self):
    self.timer = QTimer()
    self.timer.timeout.connect(self.fakePressure)
    self.timer.start(500)

  def handleButton(self, btn, btnName):
    if btn.isChecked():
      btn.setText(f'{btnName} Open')
      self.change -= .0001
    else:
      btn.setText(f'{btnName} Closed')
      self.change += .0001

if __name__ == '__main__':
  app = QApplication(sys.argv)
  app.setStyle('Fusion')
  window = Window()
  sys.exit(app.exec())
  
